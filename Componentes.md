# Definição de Componentes (parcial)
Neste Documento temos um descrição breve de todos os componentes, aqui colocaremos os componentes que foram pensados durante a fese de pré projeto. A ideia desse documento é que uma pessoa que não seja de eletrônica entenda o funcionamento de cada componente. Posteriormente será criado um arquivo que terá as informações técnicas e detalhadas de cada componento, o seu respectivo preço e anuncio de onde foi tirado esse preço.

## 1 - Subsistema de Controle:
 	
1x Orange Pi PC: 
    
    Placa responsável por realizar o processamento dos dados de entrada e saída, além de rodar o sistema opracional principal.

1x Orange pi Zero W: 

    Placa responsável por realizar a conexão do sistema Operacional secundario, com o sistema principal(que controla a esteira), a partir de uma aplicação web.

2 x Arduino Uno:
    
    Placa responsável por realizar a conexão entre módulos e circuitos controladores de entrada e saída,como módulo LDR, motores de passo, e drivers controladores de motor, e a Orange pi.
	
1x CNC Shield: 
    
    Está placa é responsável por acomodar os motores e controladores de motores, e acopla-los ao arduino.
	
3x Driver A4988:

    Estes cicuitos são responsáveis por controladr os motores de passo, fornecendo tensão e corrente ideais para o bom funcionamento dos motores NEMA17.
	 
1x Display 14 polegadas KNUP KP-TV14:

    Esse componente tem como função apresentar as infroamções para o usuário.

 1x Conversor Analógico Digital HX711:
    
    Conversor reponsável em transformar o sinal analógico da célula de carga para sinal em digital.

 2x Push Button:

    Botões responsáveis por ligar e botão de emergência.

1x Mause e teclado:

    Responsável por receber dados do operadora para o uso da máquina, como por exemplo código do kit a ser montado.

| Nome do componente | Modelo | Consumo | Preço | Quantidade |  |
|-|-|-|-|-|-|
| Orange Pi Main | Pi PC | 15W | $$$  | 1x | |
| Orange Pi Serv | Zero W| 15W | $$$ | 1x | |
| Arduino | Uno (ATM328P)| 200mW | $$$ | 2x | |
| CNC Shield | V3 | 25W | $$$ | 1x |
| Driver Motor | A4988 | 25W | $$$ | 3x |
| Display 14" | KNUP KP-TV14 | 25W | $$$ | 1x |
| Conversor AD | HX711 | 825mW| $$$ | 1x |


## 2 - Subsistema de Sensoriamento:

3x Módulo de sensor LDR : 

    Este sensor será utilizado para realizar um controle de passagem das peças, além de funcionar como uma verificação da quantidade de peças queforam despejadas.

2x Webcam: 
   
    Responsáveis por realizarem a captura de imagens para o processamento de imagem.

1x Célula de carga 5kg:

    Componente responsável para aferir a massa.

| Nome do componente | Modelo | Consumo | Preço | Quantidade |  |
|-|-|-|-|-|-|
| Sensor LDR | módulo com LM393 |  | $$$  | 3x | |
| Webcan 12Mp|  |  | $$$ | 2x | |
| Célula de carga | 5kg (any)| | $$$ | 1x | |
| Módulo de alimentação geral|  | 25W | $$$ | 1x |

## 3 - Subsistema de Motores e atuadores:
	
3x Motores Nema 17 : 

    Os motores NEMA17 são utilizados para realizar o movimento das esteiras.


## 4 - Subsistema de Alimentação:
	 
1x Proteção anti-raios e surtos DPS 20A iClamper Pocket:
    
    Um circuito de proteção com o intuito de proteger todo o sistema eletro-eletrônico, evitando queima de componetes.

1x Módulo step down LM2596

    Módulo regulador de tensão step down, de 12V para 5V. A função do módulo é filtar os ruídos da fonte 12V e gerar alimentação para o resto do sistema.

1x Fonte Chaveada 12V

    Fonte responsável pela alimentação de todo o sistema.

1x Módulo de comutação elética

    Módulo responsável pela comutação da energia da rede para o sistema de alimentação de redundância.

1x Bateria 12V 7Ah

    Responsável pela alimentação do sistema em caso de queda da rede. Sistema de redundância.

| Nome do componente | Modelo | Consumo | Preço | Quantidade |  |
|-|-|-|-|-|-|
| Fonte chaveada |  |  | $$$ | 1x |
| Proteção anti-raios e surtos DPS 20A | iClamper Pocket |  | $$$ | 1x |
| Step Down | LM2596 |  | $$$ | 1x |
| Módulo de comutação elétrica |  |  | $$$ | 1x |


Consumo total em potência 126 Wh

Consumo de corrente 15Ah

Circuito 5V

    6,091Ah 30,45Wh

Circuito 12V

    8Ah 96Wh