
# [RF]Requisitos Funcionais de Energia e Eletrônica
Neste documento montaremos os requisitos baseado na EAP criada por eletrônica, onde separararemos os requisitos nos subsistemas definidos. Após a validação desses requiisitos com toda a equipe de Energia e Eletrônica juntaremos os arquivos para os requisitos gerais do projeto.
## [RFA]Subsistema Alimentação
[RFA-01] O subsistema deverá conseguir alimentar o sistema de controle e motores. 

[RFA-02] O subsistema deverá ter um sistema de alimentação de redundância permitindo o funcionamento de todo o sistema por 5 minutos.

## [RFC]Subsistema Controle
[RFC-01] O subsistema deverá conseguir processar toda a interface de usuário, sendo ela as entradas de dados pelo usuário e saída de vídeo.

[RFC-02] O subsistema deverá conseguir processar todos os dados de entrada e saída, fornecido pelos subsistemas de Sensoriamento e de Motroes e Atuadores.

## [RFS]Subsistema Sensoriamento
[RFS-01] O subsistema deverá conseguir aferir a massa dos componentes selecionados com uma precisão rasoável.

[RFS-02] O subsistema deverá indetificar as bordas dos objetos por meio de processamento de imagem.

### [RFSA]Alto Processamento
[RFSA-01] O subsistema deverá conseguir identificar qual objeto e contar os objetos, por meio de processamento de imagem.

### [RFSB]Baixo Processamento
[RFSB-01] O subsistema deverá capaz de identificar massa de objetos a partir de célula de carga, decodificando o sinal e retornando uma saída nas unidades de medida de: Kg (Kilo gramas)ou g (gramas).

[RFSB-02] O subsistema deverá ter um sistema de redundância, para gerar robustez no sistema de Motores e Atuadores, por meio de sensores luminosos ou sonares.
## [RFM]Subsistema Motores e Atuadores
[RFM-01] O subsistema deverá ser capaz de movimentar a esteira.

# [RN]Requisitos Não Funcionais de Energia e Eletrônica
## [RNA]Subsistema Alimentação
[RNA-01] O subsistema deverá ter indicadores luminosos de funcionamento.

## [RNC]Subsistema Controle
[RNC-01] O subsistema não poderá ter travamento durante o processamento de imagem.

## [RNS]Subsistema Sensoriamento
[RNS] Não houve necessidade de requisistos não funcional.
### [RNSA]Alto Processamento
[RNSA] Não houve necessidade de requisistos não funcional.
### [RNSB]Baixo Processamento
[RNSB] Não houve necessidade de requisistos não funcional.
## [RNM]Subsistema Motores e Atuadores
[RNM-01] O subsistema dedverá indicar qual esteira está em funcionammento